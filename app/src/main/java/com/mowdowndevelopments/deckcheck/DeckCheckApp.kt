package com.mowdowndevelopments.deckcheck

import android.app.Application
import timber.log.Timber

@Suppress("unused")
class DeckCheckApp: Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant()
        }
    }
}