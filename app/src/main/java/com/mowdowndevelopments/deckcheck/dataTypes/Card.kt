package com.mowdowndevelopments.deckcheck.dataTypes

import android.os.Parcelable
import androidx.annotation.Keep
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import kotlin.math.roundToInt

@Keep
@Parcelize
@JsonClass(generateAdapter = true)
data class Card(val name: String,
                val image_uris: Map<String,String>,
                val mana_cost: String?,
                val type_line: String,
                val set: String,
                val set_name: String,
                val rarity: String,
                val power: Int?,
                val toughness: Int?,
                val loyalty: Int?,
                val oracle_text: String,
                val colors: List<Char>,
                val color_identity: List<Char>,
                val legalities: Map<String, String>,
                val prices: Map<String, Float?>,
                val scryfall_uri: String,
                val rulings_uri: String,
                val cmc: Float,
                val edhrec_rank: Int) : Parcelable {
    fun isLegal(format: String) = legalities[format] == "legal"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Card

        if (name != other.name) return false
        if (mana_cost != other.mana_cost) return false
        if (type_line != other.type_line) return false
        if (power != other.power) return false
        if (toughness != other.toughness) return false
        if (oracle_text != other.oracle_text) return false
        if (cmc != other.cmc) return false
        if (loyalty != other.loyalty) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + (mana_cost?.hashCode() ?: 0)
        result = 31 * result + type_line.hashCode()
        result = 31 * result + (power ?: 0)
        result = 31 * result + (toughness ?: 0)
        result = 31 * result + oracle_text.hashCode()
        result = (31 * result + cmc).roundToInt()
        result = 31 * result + (loyalty ?: 0)
        return result
    }

}