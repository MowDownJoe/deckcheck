package com.mowdowndevelopments.deckcheck.dataTypes

import android.net.Uri
import androidx.core.net.toUri
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CardSearchResult(val total_cards: Int,
                            val has_more: Boolean,
                            val next_page: String?,
                            val data: List<Card>){
    val nextPageUri: Uri?
        get() = next_page?.toUri()
    val maxPages = (total_cards / 30)
}

