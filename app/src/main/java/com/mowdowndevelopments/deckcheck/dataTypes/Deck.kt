package com.mowdowndevelopments.deckcheck.dataTypes

import android.os.Parcelable
import androidx.annotation.Keep
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
@JsonClass(generateAdapter = true)
data class Deck(var name: String,
                val decklist: Map<Card, Int>,
                var format: Formats
): Parcelable {
    val deckSize: Int
        get() {
            var sum = 0
            for ((_, count) in decklist){
                sum += count
            }
            return sum
        }

    val averageCMC: Double
        get() {
            var totalCMC = 0.0
            var totalNonLandCards = 0
            for ((card, count) in decklist){
                if (!card.type_line.contains("Land")){
                    totalNonLandCards += count
                    totalCMC += count * card.cmc
                }
            }
            return totalCMC / totalNonLandCards
        }
}

