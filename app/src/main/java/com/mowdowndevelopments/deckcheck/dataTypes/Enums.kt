package com.mowdowndevelopments.deckcheck.dataTypes

import java.util.*

interface EnumTools{
    fun nameAsLower(locale: Locale = Locale.getDefault()): String
}

enum class Formats(val formatName: String): EnumTools {
    STANDARD("Standard"), FUTURE("future Standard"), MODERN("Modern"), LEGACY("Legacy"),
    VINTAGE("Vintage"), PENNY("Penny Dreadful"), COMMANDER("Commander"), BRAWL("Brawl"),
    DUEL("Duel Commander"), OLDSCHOOL("Old School"), PIONEER("Pioneer");

    override fun nameAsLower(locale: Locale)= name.toLowerCase(locale)

}
enum class SortMethods: EnumTools {
    NAME, SET, RELEASED, RARITY, COLOR, USD, TIX, EUR, CMC, POWER, TOUGHNESS, EDHREC;

    override fun nameAsLower(locale: Locale) = name.toLowerCase(locale)
}

enum class SortDirections: EnumTools {
    AUTO, ASC, DESC;

    override fun nameAsLower(locale: Locale) = name.toLowerCase(locale)
}

enum class ApiStatus {
    ERROR, LOADING, DONE
}

enum class CardSizes: EnumTools {
    SMALL, MEDIUM, LARGE, PNG;

    override fun nameAsLower(locale: Locale): String = name.toLowerCase(locale)
}

