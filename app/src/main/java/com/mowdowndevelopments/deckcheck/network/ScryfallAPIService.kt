package com.mowdowndevelopments.deckcheck.network

import com.mowdowndevelopments.deckcheck.dataTypes.Card
import com.mowdowndevelopments.deckcheck.dataTypes.CardSearchResult
import com.mowdowndevelopments.deckcheck.dataTypes.SortDirections
import com.mowdowndevelopments.deckcheck.dataTypes.SortMethods
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

private const val BASE_URL = "https://api.scryfall.com/"

@Suppress("SpellCheckingInspection")
private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

private val retrofit = Retrofit.Builder().run {
    addConverterFactory(MoshiConverterFactory.create(moshi))
    baseUrl(BASE_URL)
    build()
}

interface ScryfallAPIService {
    @GET("cards/search")
    suspend fun search(@Query("q") query: String,
                       @Query("page") page: Int = 1,
                       @Query("order") sortBy: String = SortMethods.NAME.nameAsLower(),
                       @Query("dir") sortIn: String = SortDirections.AUTO.nameAsLower()) : CardSearchResult

    @GET
    suspend fun search(@Url searchUrl: String) : CardSearchResult

    @GET("cards/named")
    suspend fun fuzzyNameSearch(@Query("fuzzy") query: String,
                                @Query("set") set: String? = null) : Card

    @GET("cards/autocomplete")
    suspend fun getAutocompleteList(@Query("q") query: String): List<String>
}

object ScryfallAPI {
    val apiService: ScryfallAPIService by lazy { retrofit.create(ScryfallAPIService::class.java) }
}