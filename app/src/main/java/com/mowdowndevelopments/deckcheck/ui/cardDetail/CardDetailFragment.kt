package com.mowdowndevelopments.deckcheck.ui.cardDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.mowdowndevelopments.deckcheck.R
import com.mowdowndevelopments.deckcheck.databinding.CardDetailFragmentBinding
import kotlinx.coroutines.launch
import timber.log.Timber

class CardDetailFragment: Fragment(){
    private lateinit var viewModel: CardDetailViewModel
    private lateinit var binding: CardDetailFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val selectedCard = try {
            CardDetailFragmentArgs.fromBundle(requireArguments()).selectedCard
        } catch (e: IllegalStateException) {
            Toast.makeText(context, R.string.card_not_found_error, Toast.LENGTH_SHORT).show()
            Timber.e(e, "Args bundle was somehow missing or empty. ${e.message}")
            findNavController().popBackStack()
            return null
        }
        viewModel = ViewModelProvider(this, CardDetailViewModelFactory(selectedCard)).get(CardDetailViewModel::class.java)
        binding= DataBindingUtil.inflate(inflater, R.layout.card_detail_fragment, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val card = requireNotNull(viewModel.selectedCard.value)
        if (card.loyalty != null){
            binding.statsText.text= getString(R.string.start_loyal_stat).format(card.loyalty)
        }
        if (viewModel.selectedCard.value?.power != null){
            binding.statsText.text = getString(R.string.creature_stats)
                .format(card.power, card.toughness)
        }
        binding.cardNameText.text = card.name
        binding.manaCostText.text = card.mana_cost
        binding.cardTypeText.text = card.type_line
        binding.rulesText.text = card.oracle_text
        binding.setAndRarityText.text ="${card.set_name} (${card.rarity})"
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        lifecycleScope.launch {
            Glide.with(this@CardDetailFragment)
                .load(viewModel.selectedCard.value?.image_uris?.get("normal"))
                .placeholder(R.drawable.placeholder)
                .into(binding.appBarCardImage)
        }
    }
}