package com.mowdowndevelopments.deckcheck.ui.cardDetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mowdowndevelopments.deckcheck.dataTypes.Card

class CardDetailViewModel(card: Card): ViewModel() {

    private val _selectedCard = MutableLiveData<Card>()
    val selectedCard: LiveData<Card>
        get() = _selectedCard

    init {
        _selectedCard.value = card
    }
}