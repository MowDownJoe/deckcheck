package com.mowdowndevelopments.deckcheck.ui.cardDetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mowdowndevelopments.deckcheck.dataTypes.Card

class CardDetailViewModelFactory(private val card: Card): ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CardDetailViewModel::class.java)){
            return CardDetailViewModel(card) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}