package com.mowdowndevelopments.deckcheck.ui.deckSelect

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.mowdowndevelopments.deckcheck.R
import com.mowdowndevelopments.deckcheck.databinding.DeckSelectFragmentBinding

class DeckSelectFragment : Fragment() {

    private val viewModel by viewModels<DeckSelectViewModel> { defaultViewModelProviderFactory }
    private lateinit var binding: DeckSelectFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.deck_select_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
    }

}
