package com.mowdowndevelopments.deckcheck.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.getSystemService
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.mowdowndevelopments.deckcheck.R
import com.mowdowndevelopments.deckcheck.dataTypes.ApiStatus
import com.mowdowndevelopments.deckcheck.dataTypes.Card
import com.mowdowndevelopments.deckcheck.databinding.MainFragmentBinding
import retrofit2.HttpException
import timber.log.Timber

class MainFragment : Fragment() {

    private val viewModel by viewModels<MainViewModel> { defaultViewModelProviderFactory }
    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false)
        binding.lifecycleOwner = this

        binding.searchSubmitButton.setOnClickListener{
            try {
                viewModel.fetchCard(binding.cardNameTextBox.text.toString())
            } catch (e: IllegalArgumentException) {
                Timber.e(e, "No argument or illegal argument found.")
            } catch (he: HttpException){
                //ignore
            }
        }

        val cardObserver = Observer<Card> { newCard ->
            if (newCard != null){
                findNavController().navigate(MainFragmentDirections.actionFuzzyCardSearch(newCard))
            }
        }
        val statusObserver = Observer<ApiStatus> { newStatus ->
            if (newStatus == ApiStatus.LOADING){
                binding.searchSubmitButton.visibility = View.GONE
                binding.searchProgressBar.visibility = View.VISIBLE
                val view = activity?.currentFocus
                try {
                    requireNotNull(view)
                    val inputManager = activity?.getSystemService<InputMethodManager>()
                    requireNotNull(inputManager).hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                } catch (e: IllegalArgumentException) {
                }
            } else {
                binding.searchSubmitButton.visibility = View.VISIBLE
                binding.searchProgressBar.visibility = View.GONE
            }
        }
        val messageObserver = Observer<String> { newMessage ->
            if (newMessage!= null){
                viewModel.resetMessage()
                Snackbar.make(requireView(), newMessage, Snackbar.LENGTH_SHORT).show()
            }
        }

        viewModel.status.observe(viewLifecycleOwner, statusObserver)
        viewModel.fetchedCard.observe(viewLifecycleOwner, cardObserver)
        viewModel.errorMessage.observe(viewLifecycleOwner, messageObserver)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
    }
}
