package com.mowdowndevelopments.deckcheck.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mowdowndevelopments.deckcheck.dataTypes.ApiStatus
import com.mowdowndevelopments.deckcheck.dataTypes.Card
import com.mowdowndevelopments.deckcheck.network.ScryfallAPI
import kotlinx.coroutines.launch
import retrofit2.HttpException
import timber.log.Timber

class MainViewModel : ViewModel() {
    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status
    private val _fetchedCard = MutableLiveData<Card>()
    val fetchedCard: LiveData<Card>
        get() = _fetchedCard
    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    fun resetMessage() = _errorMessage.postValue(null)

    fun fetchCard(name: String){
        require(name.isNotEmpty()) {
            _errorMessage.postValue("Please enter a card name.")
            return
        }
        viewModelScope.launch {
            try {
                _status.postValue(ApiStatus.LOADING)
                _fetchedCard.postValue(ScryfallAPI.apiService.fuzzyNameSearch(name))
                _status.postValue(ApiStatus.DONE)
            } catch (he: HttpException){
                _status.postValue(ApiStatus.ERROR)
                if (he.code() == 404){
                    val message = "Either no cards with name $name found, or more than one found"
                    Timber.e(he, message)
                    _errorMessage.postValue(message)
                } else {
                    _errorMessage.postValue("Unknown network error.")
                    Timber.e(he, he.message())
                }
            } catch (e: Exception){
                _status.postValue(ApiStatus.ERROR)
                Timber.e(e, "Unknown error detected")
            }
        }
    }
}
